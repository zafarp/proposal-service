package com.indic.sbm.pts.interceptor;

import com.indic.sbm.pts.domain.model.ApplicationRole;
import com.indic.sbm.pts.domain.model.CurrentUser;
import com.indic.sbm.pts.domain.model.UniqueProposalId;
import com.indic.sbm.pts.domain.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
@RequiredArgsConstructor
public class ProposalInterceptor implements HandlerInterceptor {
    List<String> headerList = Arrays.asList("X_USER_ID","X_USERNAME", "X_AUTHORITY", "X_VERIFICATION",
            "X_USER_ACCESS", "X_ACCESS_SOURCE", "X_ACCESS_SOURCE_HIERARCHY");
    private final UniqueProposalId uniqueProposalId;
    private final User user;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (checkHeaderForEmptyORNullValues(request)){
            if (ApplicationRole.equals(request.getHeader("X_AUTHORITY"))) {
                 CurrentUser currentUser = CurrentUser.getInstance(
                        request.getHeader("X_USER_ID"),request.getHeader("X_USERNAME"),request.getHeader("X_AUTHORITY"),
                        request.getHeader("X_VERIFICATION"),request.getHeader("X_USER_ACCESS"),request.getHeader("X_ACCESS_SOURCE"),
                        request.getHeader("X_ACCESS_SOURCE_HIERARCHY"));
                JSONObject tst = new JSONObject(request.getHeader("X_ACCESS_SOURCE_HIERARCHY"));
                this.uniqueProposalId.setStateShortName(tst.getJSONObject("state").getString("shortName"));
                setUser(currentUser);
                return true;
            }else{
                response.setStatus(HttpStatus.UNAUTHORIZED.value()); //401
                return false;
            }
        }else{
            response.setStatus(HttpStatus.BAD_GATEWAY.value()); //502
            return false;
        }
    }

    private void setUser(CurrentUser currentUser) {
        Map<String, Object> hierarchy = new HashMap<>();
        this.user.setLoggedInAs(currentUser.getAccess().get("access").toString());
        this.user.setUserName(currentUser.getUsername());
        if (this.user.getLoggedInAs().equalsIgnoreCase("state")){
            hierarchy = (Map<String, Object>) currentUser.getHierarchy().get("state");
            this.user.setStateName(hierarchy.get("name").toString());
        }else if (this.user.getLoggedInAs().equalsIgnoreCase("ulb")){
            hierarchy = (Map<String, Object>) currentUser.getHierarchy().get("ulb");
            this.user.setUlbCode(hierarchy.get("code").toString());
        }
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }

    private boolean checkHeaderForEmptyORNullValues(HttpServletRequest request) {
        Boolean flag = true;
        for(String headerName: headerList){
            String fromReuest = request.getHeader(headerName);
            if(fromReuest != null && !fromReuest.trim().isEmpty()){
                flag = true;
            }else {
                flag = false;
                break;
            }
        }
        return flag;
    }
}

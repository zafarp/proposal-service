package com.indic.sbm.pts.domain.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Column {
    private String colKey;
    private String colName;
    private Boolean rupee;
}

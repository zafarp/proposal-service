package com.indic.sbm.pts.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Attahcment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("attachmentId")
    private Long attachmentId;
    @JsonProperty("media")
    private String media;
    @JsonProperty("url")
    private String url;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_document")
    @JsonProperty("proposal")
    @JsonBackReference("document-attachment")
    private Document document;
}

package com.indic.sbm.pts.domain.model;

import lombok.*;

import java.util.Date;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProposalDetail {
    private Long proposalId;
    private String stateName;
    private String districtName;
    private String ulbName;
    private String proposalUniqueId;
    private String sector;
    private Double proposalCost;
    private Double centralShare;
    private Double stateShare;
    private Double ulbShare;
    private Double otherShare;
    private String proposalStatus;
}

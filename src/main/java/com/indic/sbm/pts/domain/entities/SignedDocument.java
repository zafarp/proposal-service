package com.indic.sbm.pts.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SignedDocument {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("signedDocumentId")
    private Long signedDocumentId;
    @JsonProperty("media")
    private String media;
    @JsonProperty("url")
    private String url;
    @JsonBackReference("proposal-signed-document")
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_proposal")
    private Proposal proposal;
}

package com.indic.sbm.pts.domain.model;

import lombok.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
@RequestScope
public class User {
    private String loggedInAs;
    private String userName;
    private String ulbCode;
    private String ulbName;
    private String stateName;
    private String stateCode;
}

package com.indic.sbm.pts.domain.response;

import com.indic.sbm.pts.domain.entities.Proposal;
import com.indic.sbm.pts.domain.model.ProposalDetail;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class PageableProposals {
    private int currentPage;
    private Long totalItems;
    private int totalPages;
    private List<ProposalDetail> proposals;
}

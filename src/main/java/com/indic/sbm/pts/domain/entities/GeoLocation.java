package com.indic.sbm.pts.domain.entities;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeoLocation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("geoLocationId")
    private Long geoLocationId;
    @JsonProperty("wardNumber")
    private Long wardNumber;
    @JsonProperty("address")
    private String address;
    @JsonProperty("longitude")
    private String longitude;
    @JsonProperty("latitude")
    private String latitude;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_project")
    @JsonBackReference("project-location")
    @ToString.Exclude
    private Project project;
}

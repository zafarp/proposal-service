package com.indic.sbm.pts.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table
public class CentralEstimation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("centralEstimationId")
    private Long centralEstimationId;
    @JsonProperty("fundingSource")
    private String fundingSource;
    @JsonProperty("amount")
    private Double amount;
    @JsonProperty("percentageCost")
    private Double percentageCost;
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference(value = "central-financial-estimations")
    @JoinColumn(name = "fk_project")
    @ToString.Exclude
    private Project project;
}

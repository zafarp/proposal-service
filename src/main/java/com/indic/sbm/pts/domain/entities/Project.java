package com.indic.sbm.pts.domain.entities;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


/**
 * The type Project.
 */
@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@SQLDelete(sql = "UPDATE Project SET deleted=true WHERE projectId=?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted = false")
public class Project  extends Auditable<String>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("projectId")
    private Long projectId;
    @JsonProperty("uniqueProjectId")
    @Column(unique = true)
    private String uniqueProjectId;
    @JsonProperty("projectCost")
    private Double projectCost;
    @JsonProperty("projectOnM")
    private Double projectOnM;
    @JsonProperty("locationIdentified")
    private boolean locationIdentified;
    @JsonProperty("location")
    @OneToOne(mappedBy = "project", cascade = CascadeType.ALL)
    @JsonManagedReference("project-location")
    private GeoLocation location;
    @JsonProperty("ulbAmount")
    private Double ulbAmount;
    @JsonProperty("ulbPercentageCost")
    private Double ulbPercentageCost;
    @JsonProperty("centralEstimations")
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference("central-financial-estimations")
    private Set<CentralEstimation> centralEstimations = new HashSet<>();
    @JsonProperty("stateEstimations")
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference("state-financial-estimations")
    private Set<StateEstimation> stateEstimations = new HashSet<>();
    @JsonProperty("otherEstimations")
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference("other-financial-estimations")
    private Set<OtherEstimation> otherEstimations = new HashSet<>();
    @JsonProperty("plant")
    @OneToOne(mappedBy = "project", cascade = CascadeType.ALL)
    @JsonManagedReference("plant-for-project")
    private Plants plant;
    @JsonProperty("projectStatus")
    @Column(columnDefinition = "varchar(255) default 'Drafted'")
    private String projectStatus;
    @JsonProperty("document")
    @OneToOne(mappedBy = "project", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    @JsonManagedReference("project-document")
    private Document document;
    @JsonProperty("totalProjectCost")
    private Double totalProjectCost;
    @JsonProperty("deleted")
    @JsonIgnore
    private boolean deleted = false;
    @JsonProperty("projectName")
    private String projectName;
    @JsonProperty("projectDescription")
    @Lob
    private String projectDescription;
    @JsonProperty("projectDuration")
    private Long projectDuration;
    @ManyToOne
    @JoinColumn(name = "fk_proposal")
    @ToString.Exclude
    @JsonBackReference("projects-list")
    @JsonProperty("proposal")
    private Proposal proposal;


    @PreUpdate
    @PrePersist
    public void updateTotalProjectCost(){
        this.totalProjectCost = Optional.ofNullable(this.getCentralEstimations())
                .orElseGet(Collections::emptySet)
                .stream()
                .map(centralEstimation -> {
                    return centralEstimation.getAmount()!=null?centralEstimation.getAmount():0.0;
                })
                .reduce(0.0, Double::sum)
                + Optional.ofNullable(this.getStateEstimations())
                .orElseGet(Collections::emptySet)
                .stream()
                .map(stateEstimation -> {
                    return stateEstimation.getAmount()!=null?stateEstimation.getAmount():0.0;
                })
                .reduce(0.0, Double::sum)
                + Optional.ofNullable(this.getOtherEstimations())
                .orElseGet(Collections::emptySet)
                .stream()
                .map(otherEstimation -> {
                    return otherEstimation.getAmount()!=null?otherEstimation.getAmount():0.0;
                })
                .reduce(0.0, Double::sum)
                + this.getUlbAmount();
    }

    public void addDocument(Document document){
        if (document == null) {
            return;
        }
        document.setProject(this);
        this.document = document;
    }

    /**
     * Add central estimation.
     *
     * @param centralEstimation the central estimation
     */
    public void addCentralEstimation(CentralEstimation centralEstimation){
        if (centralEstimation == null) {
            return;
        }
        centralEstimation.setProject(this);
        this.centralEstimations.add(centralEstimation);
    }

    /**
     * Add state estimation.
     *
     * @param stateEstimation the state estimation
     */
    public void addStateEstimation(StateEstimation stateEstimation){
        if (stateEstimation == null) {
            return;
        }
        stateEstimation.setProject(this);
        this.stateEstimations.add(stateEstimation);
    }

    /**
     * Add other estimation.
     *
     * @param otherEstimation the other estimation
     */
    public void addOtherEstimation(OtherEstimation otherEstimation){
        if (otherEstimation == null) {
            return;
        }
        otherEstimation.setProject(this);
        this.otherEstimations.add(otherEstimation);
    }
}

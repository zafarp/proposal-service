package com.indic.sbm.pts.domain.response;

import com.indic.sbm.pts.domain.entities.Project;
import lombok.*;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ProjectCreationResponse {
         private Long projectId;
         private String projectUniqueId;
         private String plantTypeName;
         private String plantSubTypeName;
         private Double capacity;
         private Double projectCost;
         private Double projectOnM;
         private Double centralShare;
         private Double stateShare;
         private Double ulbShare;
         private Double otherShare;
}

package com.indic.sbm.pts.domain.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DetailBoxItem {
    private String label;
    private Integer value;
    private String color;
    private List<Item> items = new ArrayList<>();
}

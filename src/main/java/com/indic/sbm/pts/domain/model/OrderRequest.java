package com.indic.sbm.pts.domain.model;

import lombok.Data;

@Data
public class OrderRequest {
    private Integer orderId;
    private String paymentType;
    private Integer totalPrice;
    private Integer discount;
}

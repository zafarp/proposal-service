package com.indic.sbm.pts.domain.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SectorBreakup {
    private List<Column> columns = new ArrayList<>();
    private List<ColumnData> ColumnData = new ArrayList<>();
}

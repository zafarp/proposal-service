package com.indic.sbm.pts.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.transaction.Transactional;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
/*@NamedEntityGraph(
        name = "docs-graph",
        attributeNodes = {
                @NamedAttributeNode("attachment"),
                @NamedAttributeNode("checklist")
        }
)*/
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long documentId;
    private String documentType;
    private String documentStatus;
    private String documentStatusDate;
    private String approvalAuthority;
    @OneToOne(mappedBy = "document", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonProperty("attachment")
    @JsonManagedReference("document-attachment")
    private Attahcment attachment;
    @OneToOne(mappedBy = "document", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonProperty("checklist")
    @JsonManagedReference("document-checklist")
    private CheckList checklist;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_proposal")
    @ToString.Exclude
//    @Column(unique = true)
    @JsonBackReference(value = "proposal-document")
    private Proposal proposal;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_project")
    @ToString.Exclude
    @JsonBackReference(value = "project-document")
//    @Column(unique = true)
    private Project project;  //Project FK
}

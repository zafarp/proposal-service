package com.indic.sbm.pts.domain.response;

import com.indic.sbm.pts.domain.model.BoxItem;
import com.indic.sbm.pts.domain.model.DetailBoxItem;
import com.indic.sbm.pts.domain.model.SectorBreakup;
import com.indic.sbm.pts.domain.model.SummaryItem;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DashboardResponse {
    private List<SummaryItem> summaryItems = new ArrayList<>();
    private List<DetailBoxItem> detailBoxItems = new ArrayList<>();
    private List<BoxItem> boxItems = new ArrayList<>();
    private SectorBreakup sectorBreakup;
}

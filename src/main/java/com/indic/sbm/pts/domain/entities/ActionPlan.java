package com.indic.sbm.pts.domain.entities;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ActionPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("actionPlanId")
    private Long actionPlanId;
    @JsonProperty("media")
    private String media;
    @JsonProperty("url")
    private String url;
    @JsonProperty("plant")
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_plant")
    @JsonBackReference("plant-action-plan")
    private Plants plant;
}

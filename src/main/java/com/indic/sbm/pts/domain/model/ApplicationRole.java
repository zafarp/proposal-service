package com.indic.sbm.pts.domain.model;

import java.util.LinkedList;
import java.util.List;

public enum ApplicationRole {
    SUPER_ADMIN("Super_Admin"),
    ADMIN("Admin"),
    ANONYMOUS("ANONYMOUS_USER"),
    MANUFACTURER("Manufacturer"),
    VENDOR("Vendor"),
    CITIZEN("Citizen"),
    OFFICIAL("Official"),
    INSTITUTION("Institution"),
    ERROR("ERROR");

    private final String role;

    ApplicationRole(String role) {
        this.role = role;
    }

    public static boolean equals(String role) {
        return (role.equalsIgnoreCase("ADMIN") || role.equalsIgnoreCase("MANUFACTURER")
                || role.equalsIgnoreCase("VENDOR") || role.equalsIgnoreCase("CITIZEN")
                || role.equalsIgnoreCase("INSTITUTION") || role.equalsIgnoreCase("OFFICIAL"));
    }

    public static List<ApplicationRole> list() {
        List<ApplicationRole> roles = new LinkedList<ApplicationRole>();
        roles.add(SUPER_ADMIN);
        roles.add(ADMIN);
        roles.add(MANUFACTURER);
        roles.add(ANONYMOUS);
        roles.add(VENDOR);
        roles.add(CITIZEN);
        roles.add(OFFICIAL);
        roles.add(INSTITUTION);
        return roles;
    }

    public static List<ApplicationRole> nonOfficialUsers() {
        List<ApplicationRole> roles = new LinkedList<ApplicationRole>();
        roles.add(MANUFACTURER);
        roles.add(ANONYMOUS);
        roles.add(VENDOR);
        roles.add(CITIZEN);
        roles.add(INSTITUTION);
        return roles;
    }

    public static ApplicationRole from(String authority) {
        if (authority.equals("SUPER_ADMIN"))
            return SUPER_ADMIN;
        else if (authority.equals("ADMIN"))
            return ADMIN;
        else if (authority.equals("ANONYMOUS"))
            return ANONYMOUS;
        else if (authority.equals("MANUFACTURER"))
            return MANUFACTURER;
        else if (authority.equals("VENDOR"))
            return VENDOR;
        else if (authority.equals("CITIZEN"))
            return CITIZEN;
        else if (authority.equals("OFFICIAL"))
            return OFFICIAL;
        else if (authority.equals("INSTITUTION"))
            return INSTITUTION;
        else
            return ERROR;
    }

    public boolean isVerified() {
        return list().contains(this);
    }

    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
        return this.name();
    }
}

package com.indic.sbm.pts.domain.model;

import lombok.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
@RequestScope
public class UniqueProposalId {
    private String stateShortName;
    private Long ulbCode;
    private String sectorShortName;
    private Long primaryId;

    public String getUniqueProposalId() {
        return this.stateShortName + "/" + this.ulbCode + "/" + this.sectorShortName + "/" + this.primaryId ;
    }
}

package com.indic.sbm.pts.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

/**
 * The type Proposal.
 */
@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@SQLDelete(sql = "UPDATE Proposal SET deleted=true WHERE proposalId=?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted = false")
public class Proposal extends Auditable<String>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("proposalId")
    private Long proposalId;
    @JsonProperty("uniqueProposalId")
    @Column(unique = true)
    private String uniqueProposalId;
    @JsonProperty("ulbCode")
    private Long ulbCode;
    @JsonProperty("sector")
    private String sector;
    @JsonProperty("state")
    private String state;
    @JsonProperty("stateId")
    private Long stateId;
    @JsonProperty("districtId")
    private Long districtId;
    @JsonProperty("ulbName")
    private String ulbName;
    @JsonProperty("districtName")
    private String districtName;
    @JsonProperty("stateName")
    private String stateName;
    @JsonProperty("cityOrUlb")
    private String cityOrUlb;
    @JsonProperty("isIntegrated")
    private boolean isIntegrated;
    @JsonProperty("proposalBrief")
    @Lob
    private String proposalBrief;
    @JsonProperty("projects")
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "proposal", cascade = CascadeType.ALL)
    @JsonManagedReference("projects-list")
    private Set<Project> projects = new HashSet<>();
    @JsonProperty("gapAnalysis")
    @OneToOne(mappedBy = "proposal", cascade = CascadeType.ALL)
    @JsonManagedReference("proposal-gap-analysis")
    private Analysis gapAnalysis;
    @JsonProperty("totalProposalCost")
    private Double totalProposalCost = 0.0;
    @JsonProperty("document")
    @OneToOne(mappedBy = "proposal", cascade = CascadeType.ALL)
    @JsonManagedReference("proposal-document")
    private Document document;
    @JsonProperty("proposalStatus")
    private String proposalStatus = "Drafted";
    @OneToOne(mappedBy = "proposal", cascade = CascadeType.ALL)
    @JsonProperty("signedDocument")
    @JsonManagedReference("proposal-signed-document")
    private SignedDocument signedDocument;
    @JsonProperty("deleted")
    @JsonIgnore
    private boolean deleted = false;

    public void addAnalysis(Analysis analysis){
        if (analysis == null) {
            return;
        }
        analysis.setProposal(this);
        this.gapAnalysis = analysis;
    }

    /**
     * Add projects.
     *
     * @param project the project
     */
    public void addProjects(Project project){
        if (project == null) {
            return;
        }
        project.setProposal(this);
        this.projects.add(project);
    }
}

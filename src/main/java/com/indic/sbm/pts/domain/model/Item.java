package com.indic.sbm.pts.domain.model;


import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Item {
    private String label;
    private Double value;
    private Boolean amount;
}

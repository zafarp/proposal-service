package com.indic.sbm.pts.domain.response;

import com.indic.sbm.pts.domain.entities.Analysis;
import com.indic.sbm.pts.domain.model.ProjectIds;
import lombok.*;

import java.util.List;
import java.util.Map;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class AnalysisSubmission {
    private String uniqueProposalId;
    private Long proposalId;
    private List<ProjectIds> projects;
    private Analysis analysis;
}

package com.indic.sbm.pts.domain.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.indic.sbm.pts.domain.entities.Document;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProposalDetails {
    @JsonProperty("proposalId")
    private Long proposalId;
    @JsonProperty("proposalBrief")
    private String proposalBrief;
    @JsonProperty("isIntegrated")
    private boolean isIntegrated;
    @JsonProperty("projects")
    private List<ProjectDetails> projects = new ArrayList<>();
    @JsonProperty("document")
    private Document document;
}

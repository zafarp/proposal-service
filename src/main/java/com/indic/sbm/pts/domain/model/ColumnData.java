package com.indic.sbm.pts.domain.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ColumnData {
    private String sector;
    private String centralAllocation;
    private Long proposals;
    private Long projects;
    private String capacity;
    private String totalAmount;
    private String centralShare;
    private String stateShare;
    private String ulbShare;
    private String otherShare;
}

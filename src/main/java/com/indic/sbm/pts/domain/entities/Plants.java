package com.indic.sbm.pts.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.kie.api.definition.rule.All;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@All
@JsonInclude(JsonInclude.Include.NON_NULL)
@NamedEntityGraph(
        name = "plant-graph",
        attributeNodes = {
                @NamedAttributeNode("actionPlan")
        }
)
public class Plants {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("plantId")
    private Long plantId;
    @JsonProperty("plantTypeId")
    private String plantTypeId;
    @JsonProperty("plantTypeName")
    private String plantTypeName;
    @JsonProperty("plantSubTypeId")
    private String plantSubTypeId;
    @JsonProperty("plantSubTypeName")
    private String plantSubTypeName;
    @JsonProperty("plantCapacity")
    private Double plantCapacity;
    @JsonProperty("actionPlan")
    @OneToOne(mappedBy = "plant", cascade = CascadeType.ALL)
    @JsonManagedReference("plant-action-plan")
    private ActionPlan actionPlan;
    @JsonProperty("project")
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_project")
    @JsonBackReference("plant-for-project")
    @ToString.Exclude
    private Project project;
}

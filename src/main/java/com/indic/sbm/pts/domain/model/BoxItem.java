package com.indic.sbm.pts.domain.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BoxItem {
    private String label;
    private String value;
}

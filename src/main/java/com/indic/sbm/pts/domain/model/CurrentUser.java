package com.indic.sbm.pts.domain.model;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.indic.sbm.pts.exceptions.AuthenticationException;
import com.indic.sbm.pts.exceptions.BadRequestException;
import lombok.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CurrentUser {
    private String userId;
    private String username;
    private ApplicationRole authority;
    private Map<String, Object> verification;
    private Map<String, Object> access;
    private Map<String, Object> accessSource;
    private Map<String, Object> hierarchy;

    public static CurrentUser getInstance(String userId, String username, String authority, String verification, String access,
                                          String accessSource, String hierarchy) {
        verifyJSON(verification, access, accessSource, hierarchy);
        CurrentUser currentUser = new CurrentUser(
                userId, username,
                ApplicationRole.from(authority),
                new JSONObject(verification).toMap(),
                new JSONObject(access).toMap(),
                new JSONObject(accessSource).toMap(),
                new JSONObject(hierarchy).toMap()
        );

        if (!currentUser.getAuthority().isVerified())
            throw new AuthenticationException("Unauthorised to access this resource.");

        return currentUser;
    }

    private static void verifyJSON(String verification, String access, String accessSource, String hierarchy) {
        if (!isVerifiedJSON(verification) || !isVerifiedJSON(access)
                || !isVerifiedJSON(accessSource) || !isVerifiedJSON(hierarchy))
            throw new BadRequestException("Invalid Request Header JSON");
    }

    private static boolean isVerifiedJSON(String stringJSON) {
        try {
            new Gson().fromJson(new JSONObject(stringJSON).toString(), Object.class);
            return true;
        } catch (JsonSyntaxException | JSONException jsonException) {
            return false;
        }
    }

}

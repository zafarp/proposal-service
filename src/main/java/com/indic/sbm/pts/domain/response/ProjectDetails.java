package com.indic.sbm.pts.domain.response;

import com.indic.sbm.pts.domain.entities.Document;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProjectDetails {
    private Long projectId;
    private Document document;
}

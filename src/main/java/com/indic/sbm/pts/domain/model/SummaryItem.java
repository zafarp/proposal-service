package com.indic.sbm.pts.domain.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SummaryItem {
    private String label;
    private Double value;
    private Boolean amount; // True / False iff count or amount.
    private String color;
}

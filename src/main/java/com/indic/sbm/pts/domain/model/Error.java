package com.indic.sbm.pts.domain.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Error implements Serializable {

    public String errorMessage = "";
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    public Date timeStamp;

    public static Error setErrorResponse(String errorMessage) {
        Error error = new Error();
        error.setErrorMessage(errorMessage);
        error.setTimeStamp(new Date());
        return error;
    }

}

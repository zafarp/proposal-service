package com.indic.sbm.pts.controller.handler;

import com.indic.sbm.pts.exceptions.ClassificationException;
import com.indic.sbm.pts.domain.model.Error;
import com.indic.sbm.pts.exceptions.ProjectException;
import com.indic.sbm.pts.exceptions.ProposalException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ClassificationException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error classificationNotFound(ClassificationException exception){
        log.error("Classification not found exception.");
        String msg = "Classification Not Found.";
        return Error.setErrorResponse(getExceptionMessage(exception.getMessage(), msg));
    }

    @ExceptionHandler(ProjectException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error ProjectNotFound(ProjectException exception){
        log.error("Project not found exception.");
        String msg = "Project Not Found.";
        return Error.setErrorResponse(getExceptionMessage(exception.getMessage(), msg));
    }

    @ExceptionHandler(ProposalException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error ProposalNotFound(ProposalException exception){
        log.error("Project not found exception.");
        String msg = "Project Not Found.";
        return Error.setErrorResponse(getExceptionMessage(exception.getMessage(), msg));
    }


    private String getExceptionMessage(String message, String staticMessage) {
        return (message != null && !message.equals("")) ? message : staticMessage;
    }
}

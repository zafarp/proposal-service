package com.indic.sbm.pts.controller;

import com.indic.sbm.pts.domain.entities.Analysis;
import com.indic.sbm.pts.domain.entities.Proposal;
import com.indic.sbm.pts.domain.model.ProjectIds;
import com.indic.sbm.pts.domain.model.UniqueProposalId;
import com.indic.sbm.pts.domain.response.AnalysisSubmission;
import com.indic.sbm.pts.services.AnalysisService;
import com.indic.sbm.pts.services.ProjectService;
import com.indic.sbm.pts.services.ProposalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.stream.Collectors;


/**
 * The type Analysis controller.
 */
@RestController
@Slf4j
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping("/analysis")
public class AnalysisController {
    private final ProposalService proposalService;
    private final UniqueProposalId uniqueProposalId;
    private final AnalysisService analysisService;
    private final ProjectService projectService;

    /**
     * Save analysis response entity.
     *
     * @param analysis the analysis
     * @return the response entity
     */
    @PostMapping(consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> saveAnalysis(@RequestBody Analysis analysis) {
        Analysis analysisPersisted = analysisService.addOrUpdateAnalysis(analysis);
        Proposal proposal = new Proposal();
        Proposal proposalPersisted = proposalService.addOrUpdateProposal(proposal);
        this.uniqueProposalId.setUlbCode(analysis.getUlbId()); //
        this.uniqueProposalId.setSectorShortName(analysis.getSectorAbbr());
        this.uniqueProposalId.setPrimaryId(proposalPersisted.getProposalId());
        proposalPersisted.setUniqueProposalId(uniqueProposalId.getUniqueProposalId());
        proposalPersisted.addAnalysis(analysisPersisted);
        proposalPersisted.setUlbCode(analysis.getUlbId());
        proposalPersisted.setState(analysis.getStateName());
        proposalPersisted.setSector(analysis.getSectorAbbr());
        proposalPersisted.setUlbName(analysis.getUlbName());
        proposalPersisted.setDistrictName(analysis.getDistrictName());
        proposalPersisted.setStateName(analysis.getStateName());
        proposalPersisted.setStateId(analysis.getStateId());
        proposalPersisted.setDistrictId(analysis.getDistrictId());
        proposalService.addOrUpdateProposal(proposalPersisted);
        AnalysisSubmission analysisSubmission = AnalysisSubmission
                .builder()
                .uniqueProposalId(proposalPersisted.getUniqueProposalId())
                .projects(
                        proposalPersisted
                                .getProjects()
                                .stream()
                                .map(project -> {
                                    return new ProjectIds(project.getProjectId(), project.getUniqueProjectId());
                                })
                                .collect(Collectors.toList())
                )
                .analysis(proposalPersisted.getGapAnalysis()).proposalId(proposalPersisted.getProposalId()).build();
        return ResponseEntity.ok(analysisSubmission);
    }
}

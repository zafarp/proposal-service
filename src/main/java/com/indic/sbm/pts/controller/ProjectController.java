package com.indic.sbm.pts.controller;

import com.indic.sbm.pts.domain.entities.Document;
import com.indic.sbm.pts.domain.entities.Project;
import com.indic.sbm.pts.domain.entities.Proposal;
import com.indic.sbm.pts.domain.model.User;
import com.indic.sbm.pts.domain.response.ProjectCreationResponse;
import com.indic.sbm.pts.services.DocumentService;
import com.indic.sbm.pts.services.ProjectService;
import com.indic.sbm.pts.services.ProposalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.stream.Collectors;

/**
 * The type Project controller.
 */
@RestController
@RequestMapping("/project")
@RequiredArgsConstructor
@Slf4j
@CrossOrigin
public class ProjectController {
    private final ProposalService proposalService;
    private final ProjectService projectService;
    private final DocumentService documentService;
    private final User user;

    /**
     * Add project response entity.
     *
     * @param proposalId the proposal id
     * @param project    the project
     * @return the response entity
     */
    @PostMapping(value = "/{proposalId}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> addProject(@PathVariable Long proposalId, @RequestBody Project project){
        Proposal proposal = proposalService.fetchProposalById(proposalId).get();
        project.setProposal(proposal);
        project.setUniqueProjectId(proposal.getUniqueProposalId()+"/NA/");
        Project projectPersisted = projectService.addOrUpdateProject(project);
        projectPersisted.setUniqueProjectId(projectPersisted.getUniqueProjectId()+projectPersisted.getProjectId());
        proposal.setTotalProposalCost(projectPersisted.getTotalProjectCost() + proposal.getTotalProposalCost());
        proposalService.addOrUpdateProposal(proposal);
        return ResponseEntity.ok(this.composeProjectResponse(projectService.addOrUpdateProject(projectPersisted)));
    }

    /**
     * Get all projects only ids response entity.
     *
     * @param proposalId the proposal id
     * @return the response entity
     */
    @GetMapping("/projects/{proposalId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getAllProjectsOnlyIds(@PathVariable Long proposalId){
        return proposalService
                .fetchProposalById(proposalId)
                .map(proposal -> {
                    return ResponseEntity.ok(
                            proposal
                            .getProjects()
                            .stream()
                            .map(project -> {
                                return project.getProjectId();
                            })
                            .collect(Collectors.toList())
                    );
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Gets project by id.
     *
     * @param projectId the project id
     * @return the project by id
     */
    @GetMapping(value = "/{projectId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getProjectById(@PathVariable("projectId") Long projectId){
        return projectService
                .fetchProjectById(projectId)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Update project response entity.
     *
     * @param projectId the project id
     * @param project   the project
     * @return the response entity
     */
    @PutMapping("/{projectId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> updateProject(@PathVariable Long projectId, @RequestBody Project project){
        return projectService
                .fetchProjectById(projectId)
                .map(projObj ->{
                    Proposal proposal = projObj.getProposal();
                    project.setProjectId(projObj.getProjectId());
                    project.setUniqueProjectId(projObj.getUniqueProjectId());
                    project.setProposal(projObj.getProposal());
                    Project projectPersisted = projectService.addOrUpdateProject(project);
                    Proposal proposalFetched =projectPersisted.getProposal();
                    proposalFetched.setTotalProposalCost(projectPersisted.getTotalProjectCost() + proposalFetched.getTotalProposalCost());
                    proposalService.addOrUpdateProposal(proposalFetched);
                    log.error("Project Updation Persistance: {}", projectPersisted);
                    return ResponseEntity.ok(this.composeProjectResponse(projectService.addOrUpdateProject(projectPersisted)));
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }


    /**
     * Gets all project for proposal.
     *
     * @param proposalId the proposal id
     * @return the all project for proposal
     */
    @GetMapping(value = "/proposal/{proposalId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getAllProjectForProposal(@PathVariable("proposalId") Long proposalId){
        return ResponseEntity.ok(
                proposalService.fetchProposalById(proposalId)
                        .get()
                        .getProjects()
                        .parallelStream()
                        .filter(project -> {
                            return project!=null;
                        })
                        .map(this::composeProjectResponse)
                        .collect(Collectors.toList())
        );
    }

    /**
     * Delete project by id response entity.
     *
     * @param projectId the project id
     * @return the response entity
     */
    @DeleteMapping(value = "/{projectId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> deleteProjectById(@PathVariable("projectId") Long projectId){
        return projectService
                .fetchProjectById(projectId)
                .map(project ->{
                    projectService.deleteProjectById(projectId);
                    return ResponseEntity.ok(project);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Add individual document response entity.
     *
     * @param proposalId the proposal id
     * @param proposal   the proposal
     * @return the response entity
     */
    @PutMapping(value = "document/{proposalId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> addIndividualDocument(@PathVariable Long proposalId, @RequestBody Proposal proposal){
        return proposalService
                .fetchProposalById(proposalId)
                .map(proposalObject-> {
                    proposalObject.setProposalBrief(proposal.getProposalBrief());
                    proposalObject.setIntegrated(proposal.isIntegrated());
                    Proposal proposalPersisted = proposalService.addOrUpdateProposal(proposalObject);
                    return ResponseEntity.ok(
                            proposal
                            .getProjects()
                            .stream()
                            .map(project -> {
                                log.error("Project Logging: {} ", project.getProjectId());
                                Project projectObject = projectService.fetchProjectById(project.getProjectId()).get();
                                Document document = projectObject.getDocument();
                                projectObject.setDocument(project.getDocument());
                                return projectService.addOrUpdateProject(projectObject);
                            })
                            .collect(Collectors.toList())
                    );
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    private ProjectCreationResponse composeProjectResponse(Project project) {
        return ProjectCreationResponse
                .builder()
                .projectId(project.getProjectId())
                .projectUniqueId(project.getUniqueProjectId())
                .plantTypeName(project.getPlant().getPlantTypeName())
                .plantSubTypeName(project.getPlant().getPlantSubTypeName())
                .capacity(project.getPlant().getPlantCapacity())
                .projectCost(project.getProjectCost())
                .projectOnM(project.getProjectOnM())
                .ulbShare(project.getUlbAmount())
                .centralShare(project.getCentralEstimations().parallelStream().map(centralEstimation -> centralEstimation.getAmount()).reduce(0.0, Double::sum))
                .stateShare(project.getStateEstimations().parallelStream().map(stateEstimation -> stateEstimation.getAmount()).reduce(0.0, Double::sum))
                .otherShare(project.getOtherEstimations().parallelStream().map(otherEstimation -> otherEstimation.getAmount()).reduce(0.0, Double::sum))
                .build();
    }
}

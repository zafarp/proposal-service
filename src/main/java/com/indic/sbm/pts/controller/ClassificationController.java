package com.indic.sbm.pts.controller;

import com.indic.sbm.pts.domain.entities.Classification;
import com.indic.sbm.pts.exceptions.ClassificationException;
import com.indic.sbm.pts.services.ClassificationService;
import com.indic.sbm.pts.services.ProposalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * The type Classification controller.
 */
@RestController
@Slf4j
@RequiredArgsConstructor
@CrossOrigin
@Api(description = "For operations of creating viewing, updating and deleting the classification generically.", tags = "Classifications CRUD")
public class ClassificationController {
    private final ClassificationService classificationService;
    private final ProposalService proposalService;

    /**
     * Gets all children for parent.
     *
     * @param parentId the parent id
     * @return the all children for parent
     */
    @ApiOperation(value = "Fetches the defined Classifications.")
    @GetMapping(value={"/classification", "/classification/{parentId}"}, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<?>> getAllChildrenForParent(@PathVariable(required = false) Optional<Long> parentId){
        List<Classification> classifications;
        if (parentId.isPresent()) {
            classifications = classificationService.fetchAllChildrenForParent(parentId.get());
            checkClassificationAvailable(classifications);
        }else{
            classifications = classificationService.fetchAllChildrenForParent(0L);
            checkClassificationAvailable(classifications);
        }
        return ResponseEntity.ok(classifications);
    }

    private void checkClassificationAvailable(List<Classification> classifications) {
        if (classifications == null || classifications.isEmpty()) {
            throw new ClassificationException("Classification Not found");
        }
    }

    /**
     * Add classification response entity.
     *
     * @param classification the classification
     * @return the response entity
     */
    @ApiOperation(value = "Adds the defined Classification.")
    @PostMapping(value = "/classification", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> addClassification(@Valid @RequestBody Classification classification){
        return ResponseEntity.ok(classificationService.addOrUpdateClassification(classification));
    }

    /**
     * Update classification response entity.
     *
     * @param classification the classification
     * @return the response entity
     */
    @ApiOperation(value = "Updates the defined Classification.")
    @PutMapping(value = "/classification", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> updateClassification(@Valid @RequestBody Classification classification){
        return ResponseEntity.ok(classificationService.addOrUpdateClassification(classification));
    }
}

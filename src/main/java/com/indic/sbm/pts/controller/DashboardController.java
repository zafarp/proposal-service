package com.indic.sbm.pts.controller;

import com.indic.sbm.pts.domain.entities.Proposal;
import com.indic.sbm.pts.domain.model.*;
import com.indic.sbm.pts.domain.response.DashboardResponse;
import com.indic.sbm.pts.services.ProposalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/dashboard")
@CrossOrigin
@Slf4j
@RequiredArgsConstructor
public class DashboardController {
    private final ProposalService proposalService;
    private final User user;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getDashboard(){
        List<Proposal> proposals = null;
        if (this.user.getLoggedInAs().equals("State")) {
            proposals = proposalService.fetchAllProposalByState(this.user.getStateName());
        }else{
            proposals = proposalService.fetchAllProposalByState("CHHATTISGARH");
        }

        /**
         * Collect DAta from DB.
         * MAKE DASHBOARD RESPONSE AND RETURN;
         */

        return ResponseEntity.ok(
                DashboardResponse
                        .builder()
                        .summaryItems(buildSummaryItems())
                        .detailBoxItems(buildDetailBoxItem())
                        .boxItems(buildBoxItem())
                        .sectorBreakup(buildSectorBreakUp())
                        .build()
        );
    }

    private SectorBreakup buildSectorBreakUp() {
        return SectorBreakup
                .builder()
                .columns(buildColumns())
                .ColumnData(buildColumnData())
                .build();
    }

    private List<ColumnData> buildColumnData() {
        List<ColumnData> columns = new ArrayList<>();
        columns.add(ColumnData.builder().build());
        return columns;
    }

    private List<Column> buildColumns() {
        List<Column> columns = new ArrayList<>();
        columns.add(Column.builder().colKey("sector").colName("Sector").build());
        columns.add(Column.builder().colKey("centralAllocation").colName("Central Allocation").build());
        columns.add(Column.builder().colKey("proposals").colName("Proposals").build());
        columns.add(Column.builder().colKey("projects").colName("Projects").build());
        columns.add(Column.builder().colKey("capacity").colName("Capacity").build());
        columns.add(Column.builder().colKey("totalAmount").colName("Amount").rupee(true).build());
        columns.add(Column.builder().colKey("centralShare").colName("Central Share").rupee(true).build());
        columns.add(Column.builder().colKey("stateShare").colName("State Share").rupee(true).build());
        columns.add(Column.builder().colKey("ulbShare").colName("ULB Share").rupee(true).build());
        columns.add(Column.builder().colKey("otherShare").colName("Other Share").rupee(true).build());
        return columns;
    }

    private List<BoxItem> buildBoxItem() {
        List<BoxItem> boxItems = new ArrayList<>();
        boxItems.add(BoxItem.builder().label("Number of Proposals").value(String.valueOf(47)).build());
        boxItems.add(BoxItem.builder().label("Total Projects").value(String.valueOf(47)).build());
        boxItems.add(BoxItem.builder().label("Projects with DPR").value(String.valueOf(47)).build());
        boxItems.add(BoxItem.builder().label("No. of ULBs Submitted Proposal").value(String.valueOf(47)+ "/" + String.valueOf(55)).build());
        return boxItems;
    }

    private List<DetailBoxItem> buildDetailBoxItem() {
        List<DetailBoxItem> detailBoxItems = new ArrayList<>();
        List<Item> items = new ArrayList<>();
        items.add(Item.builder().label("Total Proposal Cost").value(0.0).amount(true).build());
        items.add(Item.builder().label("Total Central Share").value(0.0).amount(true).build());
        detailBoxItems.add(DetailBoxItem.builder().label("No. of Proposals Submitted to SHPC").value(13).color("").items(items).build());
        detailBoxItems.add(DetailBoxItem.builder().label("No. of Proposals Submitted to SHPC").value(13).color("").items(items).build());
        return detailBoxItems;
    }

    private List<SummaryItem> buildSummaryItems() {
        List<SummaryItem> summaryItems = new ArrayList<>();
            summaryItems.add(SummaryItem.builder().label("Central SBM(U) Allocation").value(0.0).amount(true).color("#116788").build());
            summaryItems.add(SummaryItem.builder().label("Requested Amount").value(0.0).amount(true).color("#2FAED5").build());
            summaryItems.add(SummaryItem.builder().label("Sanctioned Amount").value(0.0).amount(true).color("#54C49B").build());
            summaryItems.add(SummaryItem.builder().label("Utilised Amount").value(0.0).amount(true).color("#226D77").build());
            return summaryItems;
    }


}

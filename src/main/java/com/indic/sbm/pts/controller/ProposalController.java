package com.indic.sbm.pts.controller;

import com.indic.sbm.pts.domain.entities.Proposal;
import com.indic.sbm.pts.domain.enums.SearchOperation;
import com.indic.sbm.pts.domain.model.ProjectIds;
import com.indic.sbm.pts.domain.model.SearchCriteria;
import com.indic.sbm.pts.domain.model.User;
import com.indic.sbm.pts.domain.response.AnalysisSubmission;
import com.indic.sbm.pts.domain.response.PageableProposals;
import com.indic.sbm.pts.domain.response.ProjectDetails;
import com.indic.sbm.pts.domain.response.ProposalDetails;
import com.indic.sbm.pts.exceptions.ProposalException;
import com.indic.sbm.pts.repositories.specification.ProposalSpecification;
import com.indic.sbm.pts.services.DocumentService;
import com.indic.sbm.pts.services.ProposalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The type Proposal controller.
 */
@RestController
@RequestMapping("/proposal")
@CrossOrigin
@Slf4j
@RequiredArgsConstructor
public class ProposalController {
    private final ProposalService proposalService;
    private final DocumentService documentService;
    private final User user;


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getAllProposal(@RequestParam(name = "stateId", required = false) Long stateId,
                                            @RequestParam(name = "districtId", required = false) Long districtId,
                                            @RequestParam(name = "ulbId", required = false) Long ulbCode,
                                            @RequestParam(name = "sector", required = false) String sector,
                                            @RequestParam(name = "minCost", required = false) Double minCost,
                                            @RequestParam(name = "maxCost", required = false) Double maxCost,
                                            @RequestParam(name = "proposalStatus", required = false) String proposalStatus,
                                            @RequestParam(defaultValue = "10") int limit,
                                            @RequestParam(defaultValue = "0") int offset){
        ProposalSpecification spec = new ProposalSpecification();

        if (proposalStatus != null && !proposalStatus.trim().equals("")) {
            spec.add(new SearchCriteria("proposalStatus", proposalStatus, SearchOperation.EQUAL));
        }
        if (sector != null && !sector.trim().equals("")) {
            spec.add(new SearchCriteria("proposalStatus", proposalStatus, SearchOperation.EQUAL));
        }
        if (stateId != null) {
            spec.add(new SearchCriteria("stateId", stateId, SearchOperation.EQUAL));
        }
        if (districtId != null) {
            spec.add(new SearchCriteria("districtId", districtId, SearchOperation.EQUAL));
        }
        if (ulbCode != null) {
            spec.add(new SearchCriteria("ulbCode", ulbCode, SearchOperation.EQUAL));
        }
        if (sector != null && sector.trim().equals("")) {
            spec.add(new SearchCriteria("sector", sector, SearchOperation.EQUAL));
        }
        if (minCost != null) {
            spec.add(new SearchCriteria("totalProposalCost", minCost, SearchOperation.GREATER_THAN_EQUAL));
        }if (maxCost != null){
            spec.add(new SearchCriteria("totalProposalCost", maxCost, SearchOperation.LESS_THAN_EQUAL));
        }
//        spec.add(new SearchCriteria("sector", "SWM", SearchOperation.GROUP_BY));
        PageableProposals pageableProposals = proposalService.fetchAll(spec, offset, limit);
        return ResponseEntity.ok(pageableProposals);
    }

    /**
     * Get proposal response entity.
     *
     * @param proposalId the proposal id
     * @return the response entity
     */
    @GetMapping("/{proposalId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getProposal(@PathVariable Long proposalId){
        return proposalService
                .fetchProposalById(proposalId)
                .map(proposal -> {
                    return ResponseEntity.ok(
                            AnalysisSubmission
                            .builder()
                            .uniqueProposalId(proposal.getUniqueProposalId())
                            .analysis(proposal.getGapAnalysis()).proposalId(proposal.getProposalId())
                            .projects(
                                    proposal.getProjects()
                                    .stream()
                                    .map(project -> {
                                        return new ProjectIds(project.getProjectId(), project.getUniqueProjectId());
                                    })
                                    .collect(Collectors.toList())
                            )
                            .build()
                    );
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
        /*return proposalService
                .fetchProposalById(proposalId)
                .map(proposal -> {
                    return ResponseEntity.ok(proposal);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());*/
    }

    /**
     * Delete proposal response entity.
     *
     * @param proposalId the proposal id
     * @return the response entity
     */
    @DeleteMapping("/{proposalId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> deleteProposal(@PathVariable Long proposalId){
        return proposalService
                .fetchProposalById(proposalId)
                .map(proposal -> {
                    proposalService.deleteProposalById(proposalId);
                    return ResponseEntity.ok(proposal);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Add integrated document response entity.
     *
     * @param proposalId         the proposal id
     * @param integratedDocument the integrated document
     * @return the response entity
     */
    @PutMapping(value = "/document/{proposalId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> addIntegratedDocument(@PathVariable Long proposalId, @RequestBody Proposal integratedDocument){
        return proposalService
                .fetchProposalById(proposalId)
                .map(proposal -> {
                    proposal.setProposalBrief(integratedDocument.getProposalBrief());
                    proposal.setIntegrated(integratedDocument.isIntegrated());
                    proposal.setDocument(integratedDocument.getDocument());
                    return ResponseEntity.ok(proposalService.addOrUpdateProposal(proposal));
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Update proposal response entity.
     *
     * @param proposalId the proposal id
     * @param proposal   the proposal
     * @return the response entity
     */
    @PutMapping(value = "/{proposalId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> updateProposal(@PathVariable Long proposalId, @RequestBody Proposal proposal){
        return proposalService
                .fetchProposalById(proposalId)
                .map(prop -> {
                    proposal.setProposalId(proposalId);
                    return ResponseEntity.ok(proposalService.addOrUpdateProposal(proposal));
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Submit proposal response entity.
     *
     * @param proposalId the proposal id
     * @param proposal   the proposal
     * @return the response entity
     */
    @PutMapping(value = "/submit/{proposalId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> submitProposal(@PathVariable Long proposalId, @RequestBody Proposal proposal){
        return proposalService
                .fetchProposalById(proposalId)
                .map(proposalObject -> {
                     proposalObject.setProposalStatus("Submitted to State");
                     proposalObject.setSignedDocument(proposal.getSignedDocument());
                     return ResponseEntity.ok(
                             proposalService.addOrUpdateProposal(proposalObject)
                     );
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Fetch proposal for docs response entity.
     *
     * @param proposalId the proposal id
     * @return the response entity
     */
    @GetMapping(value = "/details/{proposalId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> fetchProposalForDocs(@PathVariable Long proposalId){
        return proposalService
                .fetchProposalById(proposalId)
                .map(proposal -> {
                    if (proposal.isIntegrated()) {
                        return ResponseEntity.ok(
                                ProposalDetails
                                        .builder()
                                        .proposalBrief(proposal.getProposalBrief())
                                        .document(proposal.getDocument())
                                        .isIntegrated(proposal.isIntegrated())
                                        .proposalId(proposal.getProposalId())
                                        .build());
                    }else{
                        return ResponseEntity.ok(
                                ProposalDetails
                                        .builder()
                                        .proposalId(proposal.getProposalId())
                                        .proposalBrief(proposal.getProposalBrief())
                                        .isIntegrated(proposal.isIntegrated())
                                        .projects(
                                                proposal
                                                        .getProjects()
                                                        .stream()
                                                        .map(project -> {
                                                            return ProjectDetails
                                                                    .builder()
                                                                    .projectId(project.getProjectId())
                                                                    .document(project.getDocument())
                                                                    .build();
                                                        })
                                                        .collect(Collectors.toList())
                                        )
                                        .build());
                    }
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    private Sort.Direction getSortDirection(String direction){
        if(direction.equals("asc")){
            return Sort.Direction.ASC;
        }else if(direction.equals("desc")){
            return Sort.Direction.DESC;
        }
        return Sort.Direction.ASC;
    }
}

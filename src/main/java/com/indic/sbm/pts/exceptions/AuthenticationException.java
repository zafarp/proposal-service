package com.indic.sbm.pts.exceptions;

public class AuthenticationException extends RuntimeException{

    public AuthenticationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AuthenticationException(final String message) {
        super(message);
    }

    public AuthenticationException(final Throwable cause) {
        super(cause);
    }
}

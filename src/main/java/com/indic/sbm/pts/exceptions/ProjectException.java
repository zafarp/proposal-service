package com.indic.sbm.pts.exceptions;

public class ProjectException extends RuntimeException{

    public ProjectException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ProjectException(final String message) {
        super(message);
    }

    public ProjectException(final Throwable cause) {
        super(cause);
    }
}

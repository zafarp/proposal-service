package com.indic.sbm.pts.exceptions;

public class ProposalException extends RuntimeException{

    public ProposalException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ProposalException(final String message) {
        super(message);
    }

    public ProposalException(final Throwable cause) {
        super(cause);
    }
}

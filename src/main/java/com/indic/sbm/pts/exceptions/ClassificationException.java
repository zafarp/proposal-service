package com.indic.sbm.pts.exceptions;

public class ClassificationException extends RuntimeException{
    public ClassificationException(final String message, final Throwable cause){
        super(message, cause);
    }
    public ClassificationException(final String message){
        super(message);
    }
    public ClassificationException(final Throwable cause){
        super(cause);
    }

}

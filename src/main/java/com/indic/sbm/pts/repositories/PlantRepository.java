package com.indic.sbm.pts.repositories;

import com.indic.sbm.pts.domain.entities.Plants;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantRepository extends JpaRepository<Plants, Long> {
}

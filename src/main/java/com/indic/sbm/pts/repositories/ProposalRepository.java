package com.indic.sbm.pts.repositories;

import com.indic.sbm.pts.domain.entities.Proposal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.persistence.NamedAttributeNode;
import java.util.List;

@Repository
public interface ProposalRepository extends JpaRepository<Proposal, Long>, JpaSpecificationExecutor<Proposal> {
    public List<Proposal> findByUlbCode(Long ulbCode);
//    public Page<Proposal> findByUlbCode(Long ulbCode, Pageable pageable);
    public List<Proposal> findByState(String state);
//    public Page<Proposal> findByState(String state, Pageable pageable);
}

package com.indic.sbm.pts.repositories;

import com.indic.sbm.pts.domain.entities.OtherEstimation;
import com.indic.sbm.pts.domain.entities.StateEstimation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OtherEstimationRepository extends JpaRepository<OtherEstimation, Long> {
}

package com.indic.sbm.pts.repositories;

import com.indic.sbm.pts.domain.entities.ActionPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActionPlanRepository extends JpaRepository<ActionPlan, Long> {
}

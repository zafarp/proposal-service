package com.indic.sbm.pts.repositories;

import com.indic.sbm.pts.domain.entities.Classification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassificationRepository extends JpaRepository<Classification, Long> {
    public List<Classification> findAllByParentId(Long parentId);
}

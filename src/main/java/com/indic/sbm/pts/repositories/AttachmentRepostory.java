package com.indic.sbm.pts.repositories;

import com.indic.sbm.pts.domain.entities.Attahcment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttachmentRepostory extends JpaRepository<Attahcment, Long> {
}

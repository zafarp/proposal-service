package com.indic.sbm.pts.repositories;

import com.indic.sbm.pts.domain.entities.Analysis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnalysisRepository extends JpaRepository<Analysis, Long> {
}

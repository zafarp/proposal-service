package com.indic.sbm.pts.repositories;

import com.indic.sbm.pts.domain.entities.CentralEstimation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CentralEstimationRepository extends JpaRepository<CentralEstimation, Long> {
}

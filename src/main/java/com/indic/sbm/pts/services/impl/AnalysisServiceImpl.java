package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.Analysis;
import com.indic.sbm.pts.repositories.AnalysisRepository;
import com.indic.sbm.pts.services.AnalysisService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AnalysisServiceImpl implements AnalysisService {
    private final AnalysisRepository analysisRepository;

    @Override
    public Analysis fetchAnalysisById(Long analysisId) {
        return analysisRepository.findById(analysisId).get();
    }

    @Override
    public List<Analysis> fetchAllAnalysis() {
        return analysisRepository.findAll();
    }

    @Override
    public Analysis addOrUpdateAnalysis(Analysis analysis) {
        return analysisRepository.save(analysis);
    }

    @Override
    public void deleteAnalysis(Long analysisId) {
        analysisRepository.deleteById(analysisId);
    }
}

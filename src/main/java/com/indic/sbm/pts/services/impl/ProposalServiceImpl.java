package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.Proposal;
import com.indic.sbm.pts.domain.model.ProposalDetail;
import com.indic.sbm.pts.domain.response.PageableProposals;
import com.indic.sbm.pts.repositories.ProposalRepository;
import com.indic.sbm.pts.repositories.specification.ProposalSpecification;
import com.indic.sbm.pts.services.ProposalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProposalServiceImpl implements ProposalService {
    private final ProposalRepository proposalRepository;

    @Override
    public PageableProposals fetchAll(ProposalSpecification spec, int page, int size) {
        Pageable paging = PageRequest.of(page, size);
        Page<Proposal> proposals = proposalRepository.findAll(spec, paging);
        return getProposalListing(proposals);
    }

    @Override
    public List<Proposal> fetchAllProposalByUlbCode(Long ulbCode) {
//        Pageable paging = PageRequest.of(page, size);
        List<Proposal> proposals = proposalRepository.findByUlbCode(ulbCode);
//        Page<Proposal> proposals = proposalRepository.findByUlbCode(ulbCode, paging);
        return proposals;
    }

    @Override
    public List<Proposal> fetchAllProposalByState(String state) {
//        Pageable paging = PageRequest.of(page, size);
        List<Proposal> proposals = proposalRepository.findByState(state);
        return proposals;
    }

    @Override
    public Optional<Proposal> fetchProposalById(Long proposalId) {
        return proposalRepository.findById(proposalId);
    }

    @Override
    public List<Proposal> getAllProposal() {
        return null;
    }

    @Override
    public Proposal addOrUpdateProposal(Proposal proposal) {
        return proposalRepository.save(proposal);
    }

    @Override
    public void deleteProposalById(Long proposalId) {
        proposalRepository.deleteById(proposalId);
    }


    private PageableProposals getProposalListing(Page<Proposal> proposals) {
        return PageableProposals.builder().currentPage(proposals.getNumber())
                .totalItems(proposals.getTotalElements())
                .totalPages(proposals.getTotalPages())
                .proposals(
                        proposals
                                .getContent()
                                .stream()
                                .map(this::buildProposalDetails)
                                .collect(Collectors.toList())
                )
                .build();
    }

    private ProposalDetail buildProposalDetails(Proposal proposalObject) {
        return ProposalDetail
                .builder()
                .stateName(proposalObject.getStateName())
                .districtName(proposalObject.getDistrictName())
                .ulbName(proposalObject.getUlbName())
                .proposalId(proposalObject.getProposalId())
                .proposalUniqueId(proposalObject.getUniqueProposalId())
                .sector(proposalObject.getSector())
                .proposalStatus(proposalObject.getProposalStatus())
                .stateShare(getStateShare(proposalObject))
                .otherShare(getOtherShare(proposalObject))
                .ulbShare(getUlbShare(proposalObject))
                .centralShare(getCentralShare(proposalObject))
//                .proposalCost()
                .proposalCost(getCompleteProposalCost(proposalObject))
                .build();
    }

    private Double getCompleteProposalCost(Proposal proposalObject) {
        return proposalObject.getTotalProposalCost();
                /*proposalObject.getProjects()
                .stream()
                .filter(Objects::nonNull)
                .map(project -> {
                    return project.getCentralEstimations()
                                        .stream()
                                        .map(centralEstimation -> centralEstimation.getAmount())
                                        .filter(Objects::nonNull)
                                        .reduce(0.0, Double::sum)
                            + project.getStateEstimations()
                                        .stream()
                                        .filter(Objects::nonNull)
                                        .map(stateEstimation -> stateEstimation.getAmount())
                                        .reduce(0.0, Double::sum)
                            + project.getOtherEstimations()
                                        .stream()
                                        .filter(Objects::nonNull)
                                        .map(otherEstimation -> otherEstimation.getAmount())
                                        .reduce(0.0, Double::sum)
                            + project.getUlbAmount();
                })
                .reduce(0.0, Double::sum);*/
    }

    private Double getUlbShare(Proposal proposalObject) {
        return proposalObject
                .getProjects()
                .stream()
                .map(project -> {
                    return project.getUlbAmount()!=null?project.getUlbAmount():0.0;
                })
                .reduce(0.0, Double::sum);
    }
    private Double getCentralShare(Proposal proposalObject) {
        return proposalObject
                .getProjects()
                .stream()
                .map(project -> {
                    return project.getCentralEstimations()
                            .stream()
                            .filter(Objects::nonNull)
                            .map(centralEstimation -> {
                                return centralEstimation.getAmount()!=null?centralEstimation.getAmount():0.0;
                            })
                            .reduce(0.0, Double::sum);
                })
                .reduce(0.0, Double::sum);
    }
    private Double getStateShare(Proposal proposalObject) {
        return proposalObject
                .getProjects()
                .stream()
                .map(project -> {
                    return project.getStateEstimations()
                            .stream()
                            .filter(Objects::nonNull)
                            .map(stateEstimation -> {
                                return stateEstimation.getAmount()!= null?stateEstimation.getAmount():0.0;
                            })
                            .reduce(0.0, Double::sum);
                })
                .reduce(0.0, Double::sum);
    }
    private Double getOtherShare(Proposal proposalObject) {
        return proposalObject
                .getProjects()
                .stream()
                .map(project -> {
                    return project.getOtherEstimations()
                            .stream()
                            .filter(Objects::nonNull)
                            .map(otherEstimation -> {
                                return otherEstimation.getAmount()!=null?otherEstimation.getAmount():0.0;
                            })
                            .reduce(0.0, Double::sum);
                })
                .reduce(0.0, Double::sum);
    }
}

package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.CheckList;
import com.indic.sbm.pts.repositories.CheckListRepository;
import com.indic.sbm.pts.services.CheckListService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class CheckListServiceImpl implements CheckListService {
    private final CheckListRepository checkListRepository;

    @Override
    public CheckList addOrUpdateCheckList(CheckList checkList) {
        return checkListRepository.save(checkList);
    }

    @Override
    public Optional<CheckList> fetchCheckListById(Long checkListId) {
        return checkListRepository.findById(checkListId);
    }

    @Override
    public List<CheckList> fetchAllCheckList() {
        return checkListRepository.findAll();
    }

    @Override
    public void deleteCheckListById(Long checkListId) {
        checkListRepository.deleteById(checkListId);
    }
}

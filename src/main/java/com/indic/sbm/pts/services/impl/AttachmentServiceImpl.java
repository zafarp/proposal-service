package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.Attahcment;
import com.indic.sbm.pts.repositories.AttachmentRepostory;
import com.indic.sbm.pts.services.AttachmentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class AttachmentServiceImpl implements AttachmentService {
    private final AttachmentRepostory attachmentRepostory;

    @Override
    public Attahcment addOrUpdateAttachment(Attahcment attachment) {
        return attachmentRepostory.save(attachment);
    }

    @Override
    public Optional<Attahcment> fetchAttachmentById(Long attachmentId) {
        return attachmentRepostory.findById(attachmentId);
    }

    @Override
    public List<Attahcment> fetchAllAttachment() {
        return attachmentRepostory.findAll();
    }

    @Override
    public void deleteAttachmentById(Long attachmentId) {
        attachmentRepostory.deleteById(attachmentId);
    }
}

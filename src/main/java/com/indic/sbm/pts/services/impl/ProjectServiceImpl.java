package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.Project;
import com.indic.sbm.pts.repositories.ProjectRepository;
import com.indic.sbm.pts.services.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository projectRepository;

    @Override
    public Optional<Project> fetchProjectById(Long projectId) {
        return projectRepository.findById(projectId);
    }

    @Override
    public List<Project> fetchAllProject() {
        return projectRepository.findAll();
    }

    @Override
    public Project addOrUpdateProject(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public void deleteProjectById(Long projectId) {
        Project project = projectRepository.findById(projectId).get();
        project.setDeleted(true);
        projectRepository.save(project);
//        projectRepository.deleteById(projectId);
    }

    @Override
    public Project fetchProjectByObject(Project project) {
        return null;
    }
}

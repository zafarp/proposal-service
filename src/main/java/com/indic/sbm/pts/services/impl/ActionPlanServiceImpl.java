package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.ActionPlan;
import com.indic.sbm.pts.repositories.ActionPlanRepository;
import com.indic.sbm.pts.services.ActionPlanService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ActionPlanServiceImpl implements ActionPlanService {
    private final ActionPlanRepository actionPlanRepository;

    @Override
    public ActionPlan addOrUpdateActionPlan(ActionPlan actionPlan) {
        return actionPlanRepository.save(actionPlan);
    }

    @Override
    public Optional<ActionPlan> fetchActionPlanById(Long actionPlanId) {
        return actionPlanRepository.findById(actionPlanId);
    }

    @Override
    public List<ActionPlan> fetchAllActionPlan() {
        return actionPlanRepository.findAll();
    }

    @Override
    public void deleteActionPlanById(Long actionPlanId) {
        actionPlanRepository.deleteById(actionPlanId);
    }
}

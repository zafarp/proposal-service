package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.StateEstimation;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StateEstimationService {
    public StateEstimation fetchStateEstimationById(Long stateEstimationId);
    public List<StateEstimation> fetchAllStateEstimation();
    public StateEstimation addOrUpdateStateEstimation(StateEstimation stateEstimation);
    public void deleteStateEstimationById(Long stateEstimationId);
}

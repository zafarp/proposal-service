package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.Plants;
import com.indic.sbm.pts.repositories.PlantRepository;
import com.indic.sbm.pts.services.PlantService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlantServiceImpl implements PlantService {
    private final PlantRepository plantRepository;

    @Override
    public Plants fetchPlantsById(Long plantId) {
        return plantRepository.findById(plantId).get();
    }

    @Override
    public List<Plants> fetchAllPlants() {
        return plantRepository.findAll();
    }

    @Override
    public Plants addOrUpdatePlant(Plants plants) {
        return plantRepository.save(plants);
    }

    @Override
    public void deletePlantById(Long plantId) {
        plantRepository.deleteById(plantId);
    }
}

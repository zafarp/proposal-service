package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.CheckList;

import java.util.List;
import java.util.Optional;

public interface CheckListService {
    public CheckList addOrUpdateCheckList(CheckList checkList);
    public Optional<CheckList> fetchCheckListById(Long checkListId);
    public List<CheckList> fetchAllCheckList();
    public void deleteCheckListById(Long checkListId);
}

package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.Classification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ClassificationService {
    public List<Classification> fetchAllChildrenForParent(Long parentId);
    public Classification addOrUpdateClassification(Classification classification);
    public void deleteClassificationById(Long classificationId);
}

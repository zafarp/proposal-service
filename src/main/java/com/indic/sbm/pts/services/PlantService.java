package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.Plants;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PlantService {
    public Plants fetchPlantsById(Long plantId);
    public List<Plants> fetchAllPlants();
    public Plants addOrUpdatePlant(Plants plants);
    public void deletePlantById(Long plantId);
}

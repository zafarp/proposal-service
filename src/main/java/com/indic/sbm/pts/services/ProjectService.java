package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.Project;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ProjectService {
    public Optional<Project> fetchProjectById(Long projectId);
    public List<Project> fetchAllProject();
    public Project addOrUpdateProject(Project project);
    public void deleteProjectById(Long projectId);
    public Project fetchProjectByObject(Project project);
}

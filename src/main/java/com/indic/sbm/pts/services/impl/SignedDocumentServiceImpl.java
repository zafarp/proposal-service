package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.SignedDocument;
import com.indic.sbm.pts.repositories.SignedDocumentRepository;
import com.indic.sbm.pts.services.SignedDocumentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class SignedDocumentServiceImpl implements SignedDocumentService {
    private final SignedDocumentRepository signedDocumentRepository;

    @Override
    public SignedDocument addOrUpdateSignedDocument(SignedDocument signedDocument) {
        return signedDocumentRepository.save(signedDocument);
    }

    @Override
    public Optional<SignedDocument> fetchSignedDocumentById(Long signedDocumentId) {
        return signedDocumentRepository.findById(signedDocumentId);
    }

    @Override
    public void deleteSignedDocumentById(Long signedDocumentId) {
        signedDocumentRepository.deleteById(signedDocumentId);
    }
}

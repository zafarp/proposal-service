package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.Proposal;
import com.indic.sbm.pts.domain.response.PageableProposals;
import com.indic.sbm.pts.repositories.specification.ProposalSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface ProposalService {
    public List<Proposal> fetchAllProposalByUlbCode(Long ulbCode);
//    public PageableProposals fetchAllProposalByUlbCode(Long ulbCode, int page, int size);
    public List<Proposal> fetchAllProposalByState(String state);
//    public PageableProposals fetchAllProposalByState(String state, int page, int size);
    public Optional<Proposal> fetchProposalById(Long proposalId);
    public List<Proposal> getAllProposal();
    public Proposal addOrUpdateProposal(Proposal proposal);
    public void deleteProposalById(Long proposalId);
    public PageableProposals  fetchAll(ProposalSpecification spec, int page, int size);
}

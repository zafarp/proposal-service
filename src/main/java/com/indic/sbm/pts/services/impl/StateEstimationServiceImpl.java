package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.StateEstimation;
import com.indic.sbm.pts.repositories.StateEstimationRepository;
import com.indic.sbm.pts.services.StateEstimationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StateEstimationServiceImpl implements StateEstimationService {
    private final StateEstimationRepository stateEstimationRepository;;

    @Override
    public StateEstimation fetchStateEstimationById(Long stateEstimationId) {
        return stateEstimationRepository.findById(stateEstimationId).get();
    }

    @Override
    public List<StateEstimation> fetchAllStateEstimation() {
        return stateEstimationRepository.findAll();
    }

    @Override
    public StateEstimation addOrUpdateStateEstimation(StateEstimation stateEstimation) {
        return stateEstimationRepository.save(stateEstimation);
    }

    @Override
    public void deleteStateEstimationById(Long stateEstimationId) {
        stateEstimationRepository.deleteById(stateEstimationId);
    }
}

package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.Document;
import com.indic.sbm.pts.repositories.DocumentRepository;
import com.indic.sbm.pts.services.DocumentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class DocumentServiceImpl implements DocumentService {
    private final DocumentRepository documentRepository;

    @Override
    public Document addOrUpdateDocument(Document document) {
        return documentRepository.save(document);
    }

    @Override
    public Optional<Document> fetchDocumentById(Long documentId) {
        return documentRepository.findById(documentId);
    }

    @Override
    public List<Document> fetchAllDocument() {
        return documentRepository.findAll();
    }

    @Override
    public void deleteDocumentById(Long documentId) {
        documentRepository.deleteById(documentId);
    }
}

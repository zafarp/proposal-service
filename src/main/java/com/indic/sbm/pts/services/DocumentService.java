package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.Document;

import java.util.List;
import java.util.Optional;

public interface DocumentService {
    public Document addOrUpdateDocument(Document document);
    public Optional<Document> fetchDocumentById(Long documentId);
    public List<Document> fetchAllDocument();
    public void deleteDocumentById(Long documentId);
}

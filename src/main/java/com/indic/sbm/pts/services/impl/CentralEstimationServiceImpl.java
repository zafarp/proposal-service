package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.CentralEstimation;
import com.indic.sbm.pts.repositories.CentralEstimationRepository;
import com.indic.sbm.pts.services.CentralEstimationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CentralEstimationServiceImpl implements CentralEstimationService {
    private final CentralEstimationRepository centralEstimationRepository;

    @Override
    public CentralEstimation fetchCentralEstimationById(Long centralEstimationId) {
        return centralEstimationRepository.findById(centralEstimationId).get();
    }

    @Override
    public List<CentralEstimation> fetchAllCentralEstimation() {
        return centralEstimationRepository.findAll();
    }

    @Override
    public CentralEstimation addOrUpdateCentralEstimation(CentralEstimation centralEstimation) {
        return centralEstimationRepository.save(centralEstimation);
    }

    @Override
    public void deleteCentralEstimation(Long centralEstimationId) {
        centralEstimationRepository.deleteById(centralEstimationId);
    }
}

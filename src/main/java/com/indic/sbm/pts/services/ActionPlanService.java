package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.ActionPlan;

import java.util.List;
import java.util.Optional;

public interface ActionPlanService {
    public ActionPlan addOrUpdateActionPlan(ActionPlan actionPlan);
    public Optional<ActionPlan> fetchActionPlanById(Long actionPlanId);
    public List<ActionPlan> fetchAllActionPlan();
    public void deleteActionPlanById(Long actionPlanId);
}

package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.OtherEstimation;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OtherEstimationService {
    public OtherEstimation fetchOtherEstimationById(Long otherEstimationId);
    public List<OtherEstimation> fetchAllOtherEstimations();
    public OtherEstimation addOrUpdateOtherEstimation(OtherEstimation otherEstimation);
    public void deleteOtherEstimationById(Long otherEstimationId);
}

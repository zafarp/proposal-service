package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.Analysis;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AnalysisService {

    public Analysis fetchAnalysisById(Long analysisId);
    public List<Analysis> fetchAllAnalysis();
    public Analysis addOrUpdateAnalysis(Analysis analysis);
    public void deleteAnalysis(Long analysisId);
}

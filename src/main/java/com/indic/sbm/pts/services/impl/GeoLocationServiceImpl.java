package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.GeoLocation;
import com.indic.sbm.pts.repositories.GeoLocationRepository;
import com.indic.sbm.pts.services.GeoLocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GeoLocationServiceImpl implements GeoLocationService {
    private final GeoLocationRepository geoLocationRepository;

    @Override
    public GeoLocation fetchGeoLocationById(Long geoLocationId) {
        return geoLocationRepository.findById(geoLocationId).get();
    }

    @Override
    public List<GeoLocation> fetchAllGeoLocation() {
        return geoLocationRepository.findAll();
    }

    @Override
    public GeoLocation addOrUpdateGeoLocation(GeoLocation geoLocation) {
        return geoLocationRepository.save(geoLocation);
    }

    @Override
    public void deleteGeoLocationById(Long geoLocationId) {
        geoLocationRepository.deleteById(geoLocationId);
    }
}

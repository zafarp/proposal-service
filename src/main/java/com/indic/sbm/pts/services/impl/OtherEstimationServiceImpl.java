package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.OtherEstimation;
import com.indic.sbm.pts.repositories.OtherEstimationRepository;
import com.indic.sbm.pts.services.OtherEstimationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OtherEstimationServiceImpl implements OtherEstimationService {
    private final OtherEstimationRepository otherEstimationRepository;

    @Override
    public OtherEstimation fetchOtherEstimationById(Long otherEstimationId) {
        return otherEstimationRepository.findById(otherEstimationId).get();
    }

    @Override
    public List<OtherEstimation> fetchAllOtherEstimations() {
        return otherEstimationRepository.findAll();
    }

    @Override
    public OtherEstimation addOrUpdateOtherEstimation(OtherEstimation otherEstimation) {
        return otherEstimationRepository.save(otherEstimation);
    }

    @Override
    public void deleteOtherEstimationById(Long otherEstimationId) {
        otherEstimationRepository.deleteById(otherEstimationId);
    }
}

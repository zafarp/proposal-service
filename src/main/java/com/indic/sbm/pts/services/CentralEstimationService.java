package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.CentralEstimation;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CentralEstimationService {
    public CentralEstimation fetchCentralEstimationById(Long centralEstimationId);
    public List<CentralEstimation> fetchAllCentralEstimation();
    public CentralEstimation addOrUpdateCentralEstimation(CentralEstimation centralEstimation);
    public void deleteCentralEstimation(Long centralEstimationId);
}

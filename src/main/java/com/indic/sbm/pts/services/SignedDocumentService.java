package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.SignedDocument;

import java.util.Optional;

public interface SignedDocumentService {
    public SignedDocument addOrUpdateSignedDocument(SignedDocument signedDocument);
    public Optional<SignedDocument> fetchSignedDocumentById(Long signedDocumentId);
    public void deleteSignedDocumentById(Long signedDocumentId);
}

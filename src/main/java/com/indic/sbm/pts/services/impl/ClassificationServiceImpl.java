package com.indic.sbm.pts.services.impl;

import com.indic.sbm.pts.domain.entities.Classification;
import com.indic.sbm.pts.repositories.ClassificationRepository;
import com.indic.sbm.pts.services.ClassificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClassificationServiceImpl implements ClassificationService {
    private final ClassificationRepository classificationRepository;

    @Override
    public List<Classification> fetchAllChildrenForParent(Long parentId) {
        return classificationRepository.findAllByParentId(parentId);
    }

    @Override
    public Classification addOrUpdateClassification(Classification classification) {
        return classificationRepository.save(classification);
    }

    @Override
    public void deleteClassificationById(Long classificationId) {
        classificationRepository.deleteById(classificationId);
    }
}

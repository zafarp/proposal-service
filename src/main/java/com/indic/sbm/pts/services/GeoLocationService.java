package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.GeoLocation;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GeoLocationService {
    public GeoLocation fetchGeoLocationById(Long geoLocationId);
    public List<GeoLocation> fetchAllGeoLocation();
    public GeoLocation addOrUpdateGeoLocation(GeoLocation geoLocation);
    public void deleteGeoLocationById(Long geoLocationId);
}

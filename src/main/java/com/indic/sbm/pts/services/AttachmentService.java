package com.indic.sbm.pts.services;

import com.indic.sbm.pts.domain.entities.Attahcment;

import java.util.List;
import java.util.Optional;

public interface AttachmentService {
    public Attahcment addOrUpdateAttachment(Attahcment attachment);
    public Optional<Attahcment> fetchAttachmentById(Long attachmentId);
    public List<Attahcment> fetchAllAttachment();
    public void deleteAttachmentById(Long attachmentId);
}
